#ifndef IMAGE_ROTATION_FILE_H
#define IMAGE_ROTATION_FILE_H

#include <bits/types/FILE.h>

FILE* open_file(const char* fname, const char* mode);
#endif //IMAGE_ROTATION_FILE_H
