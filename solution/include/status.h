#ifndef IMAGE_ROTATION_STATUS_H
#define IMAGE_ROTATION_STATUS_H

enum read_status {
    READ_OK = 0,
    READ_INVALID_HEADER,
    READ_IMAGE_ERROR
};
enum write_status {
    WRITE_OK = 3,
    WRITE_ERROR
};

enum open_status {
    OPEN_OK = 5,
    OPEN_ERROR
};

enum rotate_status {
    ROTATE_OK = 7,
    ROTATE_ERROR
};

void print_read_status (enum read_status rstatus);
void print_write_status (enum write_status wstatus);
void print_open_status (enum open_status ostatus);
void print_rotate_status (enum rotate_status rotateStatus);
#endif //IMAGE_ROTATION_STATUS_H
