#include "image.h"
#include "status.h"

#ifndef IMAGE_ROTATION_ROTATE_H
#define IMAGE_ROTATION_ROTATE_H
enum rotate_status rotate(struct image* source);
#endif
