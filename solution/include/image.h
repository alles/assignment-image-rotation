#include "stdint.h"
#include "stdlib.h"

#include <stdbool.h>

#ifndef IMAGE_ROTATION_IMAGE_H
#define IMAGE_ROTATION_IMAGE_H
struct pixel { uint8_t b, g, r; };

struct __attribute__((packed)) image {
    uint64_t width, height;
    struct pixel* data;
};


struct image_status {
    struct image image;
    bool valid;
};

uint8_t get_padding(uint32_t width);
struct image_status create_image(uint64_t width, uint64_t height);
void image_free(struct image* image);
#endif
