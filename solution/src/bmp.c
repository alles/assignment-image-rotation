#include "image.h"
#include "bmp.h"
#include "status.h"

#include <stdint.h>
#include <stdio.h>

struct header_status {
    bmpHeader header;
    bool valid;
};

struct header_status read_header(FILE* in) {
    bmpHeader header;
    bool valid = true;
    if (fread(&header, sizeof(bmpHeader), 1, in) != 1) valid = false;
    return (struct header_status) {
            .header = header,
            .valid = valid
    };
}

static enum read_status read_image(FILE* in, bmpHeader header, struct image* img) {
    uint8_t padding = get_padding(header.biWidth);
    struct image_status imageStatus = create_image(header.biWidth, header.biHeight);
    if (!imageStatus.valid) return READ_IMAGE_ERROR;
    *img = imageStatus.image;
    for (uint64_t i = 0; i < img->height; i++) {
        if (fread(img->data + i * img->width, sizeof(struct pixel), img->width, in) != img->width)
            return READ_IMAGE_ERROR;
        if (fseek(in, padding, SEEK_CUR) != 0)
            return READ_IMAGE_ERROR;
    }
    return READ_OK;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct header_status headerStatus = read_header(in);
    if (!headerStatus.valid) return READ_INVALID_HEADER;
    if (read_image(in, headerStatus.header, img) != READ_OK) return READ_IMAGE_ERROR;
    return READ_OK;
}

static bmpHeader make_header(const uint64_t width, const uint64_t height) {
    bmpHeader header;
    header.bfType = 0x4D42;
    header.bfileSize = height * width * sizeof(struct pixel) + sizeof(bmpHeader);
    header.bfReserved = 0;
    header.bOffBits = 54;
    header.biSize = 40;
    header.biWidth = width;
    header.biHeight = height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = (sizeof(struct pixel) * width + get_padding(width)) * height;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
    return header;
}

static enum write_status write_header(FILE* out, bmpHeader header) {
    if(fwrite(&header, sizeof(bmpHeader), 1, out) != 1)
        return WRITE_ERROR;
    return WRITE_OK;
}

static enum write_status write_image(FILE* out, bmpHeader header, struct image const* img ) {
    uint8_t padding = get_padding(header.biWidth);
    uint32_t zdata = 0;
    for (uint64_t i = 0; i < img->height; i++) {
        if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out) != img->width)
            return WRITE_ERROR;
        if (fwrite(&zdata, 1, padding, out) != padding)
            return WRITE_ERROR;
    }
    return WRITE_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    bmpHeader header = make_header(img->width, img->height);
    if (write_header(out, header) != WRITE_OK) return WRITE_ERROR;
    if (write_image(out, header, img) != WRITE_OK) return WRITE_ERROR;
    return WRITE_OK;
}



