#include "status.h"

#include <stdio.h>

char* err_msg[] = {
        [READ_INVALID_HEADER] = "Header reading error",
        [READ_IMAGE_ERROR] = "Image reading error",
        [WRITE_ERROR] = "Write error",
        [OPEN_ERROR] = "File open error",
        [ROTATE_ERROR] = "Rotate error"
};

void print_read_status (enum read_status rstatus) {
    fprintf(stderr, "%s", err_msg[rstatus]);
}

void print_write_status (enum write_status wstatus) {
    fprintf(stderr, "%s", err_msg[wstatus]);
}

void print_open_status (enum open_status ostatus) {
    fprintf(stderr, "%s", err_msg[ostatus]);
}

void print_rotate_status (enum rotate_status rotateStatus) {
    fprintf(stderr, "%s", err_msg[rotateStatus]);
}
