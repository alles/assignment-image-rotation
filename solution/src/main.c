#include "image.h"
#include "bmp.h"
#include "file.h"
#include "rotate.h"
#include "status.h"

#include <stdio.h>


int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Input is not correct\nUsage: %s <source-image> <transformed-image>", argv[0]);
        exit(1);
    }

    FILE* in = open_file(argv[1], "rb");
    if (in == NULL) {
        print_open_status(OPEN_ERROR);
        exit(EXIT_FAILURE);
    }

    FILE* out = open_file(argv[2], "wb");
    if (out == NULL) {
        print_open_status(OPEN_ERROR);
        fclose(in);
        exit(EXIT_FAILURE);
    }

    struct image img;

    enum read_status readStatus = from_bmp(in, &img);
    if (readStatus != READ_OK) {
        print_read_status(readStatus);
        fclose(in);
        fclose(out);
        exit(EXIT_FAILURE);
    }
    fclose(in);

    enum rotate_status rotateStatus = rotate(&img);
    if (rotateStatus != ROTATE_OK) {
        print_rotate_status(rotateStatus);
        fclose(out);
        image_free(&img);
        exit(EXIT_FAILURE);
    }

    enum write_status writeStatus = to_bmp(out, &img);
    if (writeStatus != WRITE_OK) {
        print_write_status(writeStatus);
        fclose(out);
        image_free(&img);
        exit(EXIT_FAILURE);
    }
    fclose(out);

    image_free(&img);
    return 0;
}

