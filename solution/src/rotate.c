#include "rotate.h"
#include "status.h"

enum rotate_status rotate(struct image* source){
    uint64_t width = source->width;
    uint64_t height = source->height;
    struct image_status imageStatus = create_image(height, width);
    if (!imageStatus.valid) return ROTATE_ERROR;
    struct image transformed = imageStatus.image;
    for(uint64_t i = 0; i<height; i++){
        for(uint64_t j = 0; j < width; j++){
            transformed.data[transformed.width * j + (transformed.width - i - 1)] = source->data[i * width + j];
        }
    }
    source->width = transformed.width;
    source->height = transformed.height;
    free(source->data);
    source->data = transformed.data;
    return ROTATE_OK;
}
