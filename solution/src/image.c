#include "image.h"

#include <stdint.h>
#include <stdlib.h>

uint8_t get_padding(uint32_t width) {
    uint8_t padding = 0;
    while((width * sizeof(struct pixel) + padding) % 4 != 0)
        padding++;
    return padding;
}

struct image_status create_image(uint64_t width, uint64_t height) {
    struct image image ;
    image.width = width;
    image.height = height;
    image.data = malloc(sizeof(struct pixel) * width * height);
    bool valid = true;
    if (!image.data) valid = false;
    return (struct image_status) {
            .image = image,
            .valid = valid
    };
}

void image_free(struct image* image) {
    if(image != NULL) free(image->data);
}
