#include "file.h"

#include <stdio.h>

FILE* open_file(const char* fname, const char* mode) {
    return fopen(fname, mode);
}
